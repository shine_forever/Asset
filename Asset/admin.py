__author__ = 'shine_forever'
#!coding:utf-8

from django.contrib import admin
from models import asset_system

class serveradm(admin.ModelAdmin):
    list_display = ('ip_info','serv_info','cpu_info','disk_info','mem_info','load_info','mark_info')
    search_fields = ('ip_info','serv_info')
admin.site.register(asset_system,serveradm)